# MCL2 Resource Pack Converter
This is a cross-platform Python 3 script that can convert *almost* any recent Minecraft resource pack into a MineClone2 texture pack.

## Requirements
The script requires Python 3 installed along with Image Magick version 7 or 6.[ The Python Image Library (it's fork Pillow) must be installed as well. ](https://pillow.readthedocs.io/en/stable/installation.html)

## Usage
	python3 convert-mc-resource-pack.py [OPTIONS] INPUT-RESOURCE-PACK.zip [OUTPUT-NAME]
		INPUT-RESOURCE-PACK does not have to be in a .zip file. If it is unzipped already then that is ok. If it is zipped, this script will unzip it into this directory.
		OUTPUT-NAME is optional, if no output name is given, the resulting MineClone2 texture pack will be written to $OUTPUT_DIR, or set to the value of .pack.name in the pack.mcmeta file.
		OPTIONS may be one of:
			-h --help	Display this usage text.
			-v --version	Display the version of this script and then exit.

## Supported Features
* Almost full coverage of Minecraft blocks and items to MCL2 items and blocks
* Automatic detection of animated items and blocks being cropped to static frames for MCL2
* Conversion of the block breaking animation (with alpha values!)
* Colorization of grass and leaves

## Unsupported Features
* Cropping of mobs, chests, and armor still unsupported.
* Does not convert normal and specular maps.

## Tested Platforms
Windows 10, Image Magick version 7  
Linux Debian, Image Magick version 6

## Roadmap
1. Enable conversion of normal and specular maps.
2. Cropping of single chests
3. Conversion of ender chests
4. Conversion of double chests
5. Cropping of armor

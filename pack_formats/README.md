# pack_versions Directory
Every so often Minecraft revises it's resource pack structure and item/block IDs.  

To allow for almost any texture pack to work with this convsion tool, this directory contains CSVs with item/block names and paths for alomst every possible resource pack layout.
Currently MC has 4 resource pack versions.  

Version 4 is the most current version (1.14).  

In the explanation below, please consider the # to be the MC resource pack version number (either 1, 2, 3, or 4)  
Each #.csv file contains a list of images in the following format:   
`Source path,Source file,Target path,Target file,xs,ys,xl,yl,xt,yt,Blacklisted?`  
If blacklisted is set to "y", the entry is skippped.  
If `xs` is present, the image is cropped using ImageMagick starting from point `xs`,`ys` with dimensions of `xl` by `yl`.   

If the given source file is not found, the converted will search for alternative item IDs in the `alt-#.csv` file and look for images with those IDs.  
If no images are found with the alternative IDs, the image is skipped and a warning is emitted.   

Once normal images have been cropped and copied, greyscale images (grass, leaves) will be colorized.  
Images to be colorized are listed in the `colorize-#.csv` with the format:  
`Source path, source file, colormap file, colormap point, destination path, destination file`  
These colorized images won't try to resolve ID's with the `alt-#.csv`, so if any files are not found, a warning is emitted and the entry is skipped.